import pprint

word = {'number of letters': '4', 'grammar': 'noun', 'position': '1st'}

r = 4
print('test idea \
or something')
      
for v in word.values():
    print(v + '\n')
    
for k in word.keys():
    print(k)
    
for i in word.items():
    print(i)

keylist = list(word.keys())

print('though' in word.values())

print('there are ' + str(word.get('number of words ', 0)) + ' letters')

#if 'sentence place' not in word:
#    word['beginning'] = '1st'
    
word.setdefault('sentence place','1st')

message = 'this is a message'
count={}

for character in message:
    count.setdefault(character,0)
    count[character] = count[character]+1

# https://www.tutorialspoint.com/pprint-module-data-pretty-printer 
pp = pprint.PrettyPrinter(width=1)
pp.pprint(count)

