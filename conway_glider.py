import random, time, copy

W = 60
H = 20

nextCells = []
for x in range(W):
    col = []
    for y in range(H):
        # if random.randint(0,1)==0:
        if (x, y) in ((1, 0), (2, 1), (0, 2), (1, 2), (2, 2)):  # glider variant
            col.append('#')
        else:
            col.append(' ')
    nextCells.append(col)

while True:
    print('\n\n\n\n\n')
    currentCells = copy.deepcopy(nextCells)

    for y in range(H):
        for x in range(W):
            print(currentCells[x][y], end='')
        print()

    for x in range(W):
        for y in range(H):
            leftCoor = (x - 1) % W
            rightCoor = (x + 1) % W
            aboveCoor = (y - 1) % H
            belowCoor = (y + 1) % H

            neighbours = 0
            if currentCells[leftCoor][aboveCoor] == '#':
                neighbours += 1
            if currentCells[x][aboveCoor] == '#':
                neighbours += 1
            if currentCells[rightCoor][aboveCoor] == '#':
                neighbours += 1
            if currentCells[leftCoor][y] == '#':
                neighbours += 1
            if currentCells[rightCoor][y] == '#':
                neighbours += 1
            if currentCells[leftCoor][belowCoor] == '#':
                neighbours += 1
            if currentCells[x][belowCoor] == '#':
                neighbours += 1
            if currentCells[rightCoor][belowCoor] == '#':
                neighbours += 1

            if currentCells[x][y] == '#' and (neighbours == 2 or neighbours == 3):
                nextCells[x][y] = '#'
            elif currentCells[x][y] == ' ' and (neighbours == 3):
                nextCells[x][y] = '#'
            else:
                nextCells[x][y] = ' '
    time.sleep(2)
