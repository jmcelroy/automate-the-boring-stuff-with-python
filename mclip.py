#!/usr/bin/env python3
# mclip.py
"""
Created on Fri Jun  3 16:44:23 2022

@author: jmcelroy
"""

TEXT = {'agree': """Yes, I agree. That sounds fine.""",
        'busy': """Sorry, can we do this later this week?""",
        'upsell': """Would you consider monthly donation?"""
        }

import sys
if len(sys.argv) < 2:
    print('Usage: python mclip.py [keyphrase] - copy phrase text')
    sys.exit()
    
keyphrase = sys.argv[1]

import pyperclip

if keyphrase in TEXT:
    pyperclip.copy(TEXT[keyphrase])
    print('Text for ' + keyphrase + ' copied to keyboard.')
else:
    print('There is no text for ' + keyphrase)
    
