#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 16 16:50:33 2022

@author: jmcelroy
"""

# import traceback
import logging

#raise Exception('Error')

#try:
#    raise Exception('Error message')
    
#except:
#    errorFile = open('errorInfo.txt', 'w')
#    errorFile.write(traceback.format_exc())
#    errorFile.close()
#    print('Traceback info written to errorInfo.txt')
    
# use python -0 script.py to run while skipping assert statements

for handler in logging.root.handlers[:]:
    logging.root.removeHandler(handler) # corrects lack of logging output file 
#https://stackoverflow.com/questions/35898160/logging-module-not-writing-to-file?rq=1
    
logging.basicConfig(filename='ErrorLog4.md', level=logging.DEBUG, 
                    format=' %(asctime)s - %(levelname)s - %(message)s')
logging.debug('Start of program')


def factorial(n):
    logging.debug('Start of factorial(%s)'  % (n))
    total = 1
    for i in range(1, n+1):
        total *= i
        logging.debug('i is ' + str(i) + ', total is ' + str(total))
    logging.debug('End of factorial(%s)'  % (n))
    return total

print(factorial(5))
logging.debug('End of program.')
