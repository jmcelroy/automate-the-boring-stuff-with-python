# -*- coding: utf-8 -*-

import requests, bs4
from lxml.html import parse


#res = requests.get('https://nostarch.com')
#res.raise_for_status()
#noStarchSoup = bs4.BeautifulSoup(res.text, 'html.parser')
#print(type(noStarchSoup()))

#exampleFile = open('example.html')
exampleSoup = parse('example.html').getroot()
#exampleSoup = bs4.BeautifulSoup(exampleFile, 'lxml.parser')
#print(type(exampleSoup()))

for link in exampleSoup:
    print(f"{link.text_content()}")
    
