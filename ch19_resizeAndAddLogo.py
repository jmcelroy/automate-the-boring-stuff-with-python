#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 25 16:05:22 2022

@author: jmcelroy

The original code required modifications because the logo size
was much larger than the image files. 
"""

import os
from PIL import Image

SQUARE_FIT_SIZE = 3000
LOGO_FILENAME = 'catlogo.png'

logoIm = Image.open(LOGO_FILENAME)
original_logoWidth, original_logoHeight = logoIm.size
#logoWidth, logoHeight = logoIm.size
#logoWidth = int(logoWidth/20)
#logoHeight = int(logoHeight/20)
ninth_sized_Logo = logoIm.resize((int(original_logoWidth/3), int(original_logoHeight/3)))
logoWidth, logoHeight = ninth_sized_Logo.size

os.makedirs('withLogo', exist_ok=True)

for filename in os.listdir('.'):
    if not (filename.endswith('.png') or filename.endswith('.jpg')) or filename == LOGO_FILENAME:
        continue
    
    im = Image.open(filename)
    width, height = im.size
    
    if width > SQUARE_FIT_SIZE and height > SQUARE_FIT_SIZE:
        if width > height:
            height = int((SQUARE_FIT_SIZE/width)*height)
            width=SQUARE_FIT_SIZE
        else:
            width = int((SQUARE_FIT_SIZE/height)*width)
            height=SQUARE_FIT_SIZE
            
        print('Resizing %s...' % filename)
        im = im.resize((width, height))
        
    # add logo
    print('Adding logo to %s...' % filename)
    
    logo_x = width-logoWidth
    logo_y = height-logoHeight
    im.paste(ninth_sized_Logo, (logo_x, logo_y), ninth_sized_Logo)
    im.save(os.path.join('withLogo', filename))
    
    