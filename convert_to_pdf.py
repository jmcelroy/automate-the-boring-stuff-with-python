#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 21 17:08:33 2022

@author: jmcelroy
"""

import subprocess

def convert_to_pdf(input_doc, output_dir):
    subprocess.call(['soffice',
                     '--convert-to',
                     'pdf',
                     '%s' % input_doc,
                     '--outdir',
                     '%s' % output_dir]) 