#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 17 13:09:13 2022

@author: jmcelroy
"""

import requests

res = requests.get('https://automatetheboringstuff.com/files/rj.txt')
print(type(res))
res.status_code == requests.codes.ok
print(len(res.text))
print(res.text[:250])

# HTTP ok = 200

res2 = requests.get('https://inventwithpython.com/page_that_does_not_exist')
try:
    print(res2.raise_for_status())
except Exception as exc:
    print('There was a problem: %s' % (exc))

res.raise_for_status()
playFile = open('RomeoAndJuliet.txt', 'wb')
for chunk in res.iter_content(100000):
    playFile.write(chunk)
playFile.close()

