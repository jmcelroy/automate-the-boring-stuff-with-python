#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 21 16:36:03 2022

@author: jmcelroy
"""

import docx
from pathlib import Path
from convert_to_pdf import *

doc = docx.Document()
doc.add_paragraph('Hello, world!', 'Title')

paraObj1 = doc.add_paragraph('Add after hello world')
paraObj1.add_run(' This comes after the addition').add_break()

doc.add_paragraph('Text after line break')

doc.add_heading('Header 2', 3)

doc.add_paragraph('THis is more text.')
doc.paragraphs[3].runs[0].add_break(docx.enum.text.WD_BREAK.PAGE)

doc.add_paragraph('This is on page 2')

doc.add_picture('xkcd/kilogram.png', width=docx.shared.Cm(8), height=docx.shared.Cm(12))

doc.save('helloworld.docx')

convert_to_pdf('helloworld.docx', Path.cwd() /'testing_dir01/')