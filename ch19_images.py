#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 25 14:04:20 2022

@author: jmcelroy
"""

from PIL import ImageColor, Image
import os

#print(ImageColor.getcolor('orange', 'RGBA'))
#print(ImageColor.getcolor('chocolate', 'RGBA'))

catIm = Image.open('zophie.png')
#print(catIm.size)
#print(catIm.format)

width, height = catIm.size
catIm.save('zophie.jpg')

im1 = Image.new('RGBA', (100, 200), 'purple')
im1.save('purple.png')
im2 = Image.new('RGBA', (20, 20))
im2.save('transparent.png')

croppedIm = catIm.crop((335, 345, 565, 560))
croppedIm.save('cropped.png')

catImCopy = catIm.copy()
catImCopy.paste(croppedIm, (0, 0))
catImCopy.paste(croppedIm, (400, 500))
catImCopy.save('pasted.png')

catImWidth, catImHeight = catIm.size
faceImWidth, faceImHeight = croppedIm.size
catCopyTwo = catIm.copy()
for left in range(0, catImWidth, faceImWidth):
    for top in range(0, catImHeight, faceImHeight):
        print(left, top)
        catCopyTwo.paste(croppedIm, (left, top))
catCopyTwo.save('tiled_cat.png')

quartersizedIm = catIm.resize((int(width/2), int(height/2)))
quartersizedIm.save('quartersized.png')

catIm.rotate(90).save('rotated90.png')
catIm.rotate(6, expand=True).save('rotated6expanded.png')
#catIm.transpose(Image.FLIP_LEFT_RIGHT).save('horizontal-flip.png')

catIm.transpose(Image.Transpose.FLIP_LEFT_RIGHT).save('new-transpose-syntax.png')
# corrects depreciation warning for FLIP_LEFT_RIGHT

