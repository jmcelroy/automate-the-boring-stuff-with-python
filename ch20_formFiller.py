#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun 26 11:44:49 2022

@author: jmcelroy
"""

import pyautogui, time

from selenium import webdriver
from selenium.webdriver.firefox.service import Service

option = webdriver.FirefoxOptions()
option.binary_location = r'/usr/bin/firefox'
driverService = Service('/usr/local/bin/geckodriver')
driver = webdriver.Firefox(service=driverService, options=option)

browser = driver
browser.get('https://autbor.com/form')
browser.switch_to.window(browser.current_window_handle)
browser_info = browser.capabilities
print('PID of the browser is ' + str(browser_info['moz:processID']))
#browser.maximize_window()
        
formData = [{'name': 'Alice', 'fear': 'eavesdroppers', 'source': 'wand', 
             'robocop': 4, 'comments': 'Tell Bob I said hi.'},
            {'name': 'Bob', 'fear': 'bees', 'source': 'amulet', 
             'robocop': 4, 'comments': 'n/a'},
            {'name': 'Carol', 'fear': 'puppets', 'source': 'crystal ball', 
             'robocop': 1, 'comments': 'Please take the puppets out of the break room.'},
            {'name': 'Alex Murphy', 'fear': 'ED-209', 'source': 'money', 
             'robocop': 5, 'comments': 'Protect the innocent. \
             Serve the public trust. Uphold the law.'},
            ]   

pyautogui.PAUSE = 0.5
print('Ensure that the browser window is active and the form is loaded.')

for person in formData:
    print('>>> 3-second paause to ctrl-c <<<')
    time.sleep(3)
    
    print('Entering %s info...' % (person['name']))
    pyautogui.write(['\t', '\t', '\t', '\t'])
    pyautogui.write(person['name'] + '\t')
    pyautogui.write(person['fear'] + '\t')
    
    if person['source'] == 'wand':
        pyautogui.write(['down', '\n', '\t'], 0.5)
    elif person['source'] == 'amulet':
        pyautogui.write(['down', 'down', '\n', '\t'], 0.5)
    elif person['source'] == 'crystal ball':
        pyautogui.write(['down', 'down', 'down', '\n', '\t'], 0.5)
    elif person['source'] == 'money':
        pyautogui.write(['down', 'down', 'down', 'down', '\n', '\t'], 0.5)
        
    if person['robocop'] == 1:
        pyautogui.write([' ', '\t', '\t'], 0.5)
    elif person['robocop'] == 2:
        pyautogui.write(['right', '\t', '\t'], 0.5)
    elif person['robocop'] == 3:
        pyautogui.write(['right', 'right', '\t', '\t'], 0.5)
    elif person['robocop'] == 4:
        pyautogui.write(['right', 'right', 'right', '\t', '\t'], 0.5)
    elif person['robocop'] == 5:
        pyautogui.write(['right', 'right', 'right', 'right', '\t', '\t'], 0.5)
        
    pyautogui.write(person['comments'] + '\t')
    time.sleep(0.5) 
    pyautogui.press('enter')
    print('Submitted form.')
    time.sleep(2)
    
    pyautogui.write(['\t', '\n'])
        
    