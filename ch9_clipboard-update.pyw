#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 15 06:51:58 2022

@author: jmcelroy
"""

# allows saving new strings to clipboard program for future retrieval

import shelve, pyperclip, sys

mcbShelf = shelve.open('mcb')

entries = list(mcbShelf.keys())

if len(sys.argv) == 3 and sys.argv[1].lower() == 'save':
    mcbShelf[sys.argv[2]] = pyperclip.paste()
elif len(sys.argv) == 2:
    if sys.argv[1].lower() == 'list':
        pyperclip.copy(str(entries))
    elif sys.argv[1] in mcbShelf:
        pyperclip.copy(mcbShelf[sys.argv[1]])
    elif sys.argv[1].lower() == 'cleardict':
        for entry in entries:
            del mcbShelf[entry]
elif len(sys.argv) == 3 and sys.argv[1].lower() == 'deleteone':
    del mcbShelf[sys.argv[2]]

mcbShelf.close()