#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 24 17:49:17 2022

@author: jmcelroy
"""

import requests, os, bs4, threading
os.makedirs('xkcd', exist_ok=True)

def downloadXKCD(startComic, endComic):
    for urlNum in range(startComic, endComic):
        print('Downloading page https://xkcd.com/%s...' % urlNum)
        res = requests.get('https://xkcd.com/%s' % urlNum)
        res.raise_for_status()
        
        soup = bs4.BeautifulSoup(res.text, 'html.parser')
        
        comicElem = soup.select('#comic img')
        if comicElem == []:
            print('Could not find comic image.')
        else:
            comicUrl = comicElem[0].get('src')
            print('Downloading image %s...' % comicUrl)
            res = requests.get('https:' + comicUrl)
            res.raise_for_status()
            
            imageFile = open(os.path.join('xkcd', os.path.basename(comicUrl)), 'wb')
            
            for chunk in res.iter_content(100000):
                imageFile.write(chunk)
                
            imageFile.close()
            
downloadThreads = []
for i in range(2400, 2650, 10):
    start = i
    end = i+9
    if start == 0:
        start = 1
    downloadThread = threading.Thread(target=downloadXKCD, args=(start, end))
    downloadThreads.append(downloadThread)
    downloadThread.start()
    
for downloadThread in downloadThreads:
    downloadThread.join()
print('Done.')