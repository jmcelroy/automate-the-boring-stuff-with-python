#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 24 16:47:31 2022

@author: jmcelroy
"""

import time

#print(time.time()) # seconds since Unix epoch

print(time.ctime())

#for i in range(3):
#    print('Tick')
#    time.sleep(1)
#    print('Tock')
#    time.sleep(1)
    
#now = time.time()
#thismoment = round(now)
#print(thismoment)

import datetime
#print(datetime.datetime.now().hour)

halloween2022 = datetime.datetime(2022, 10, 31, 0, 0, 0)
newyears2023 = datetime.datetime(2023, 1, 1, 0, 0, 0)
#print(newyears2023 < halloween2022)

delta = datetime.timedelta(days=1,hours=-1,minutes=24,seconds=22)
#print(delta.total_seconds())

# negative hours possible ^ 

today = datetime.datetime.now()
thirtyYearsLater = datetime.timedelta(days=365*30)
thepast = today - thirtyYearsLater
#print(thepast)
#print(today.strftime('%Y%m%d %H:%M:%S'))
#print(today.strftime('%I:%M %p'))

#print(datetime.datetime.strptime('June 24, 2022', '%B %d, %Y'))

import threading # By default, programs single-threaded
print('Start')
def takeANap():
    time.sleep(5)
    print('Wake up!')
    
threadObj = threading.Thread(target=takeANap)
threadObj.start()
print('End')

threadObj2 = threading.Thread(target=print, args=['Cats', 'Dogs'], kwargs={'sep': ' & '})
# sep adds "&" between args
threadObj2.start()

