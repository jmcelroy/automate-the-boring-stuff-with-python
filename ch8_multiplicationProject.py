#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 13 17:43:22 2022

@author: jmcelroy
"""

# pg. 196
import pyinputplus as pyip, random, time

numQuest = 10
corrAns = 0
for questionNumber in range(numQuest):
    num1 = random.randint(0, 9)
    num2 = random.randint(0, 9)
    prompt = '#%s: %s x %s = ' % (questionNumber, num1, num2)
    
    try:
        pyip.inputStr(prompt, allowRegexes=(['^%s$' % (num1*num2)]),
                      blockRegexes=([('.*', 'Incorrect.')]),
                      timeout=(8),
                      limit=3)
        # must begin/end with corrAns, all other strings blocked
    except pyip.TimeoutException:
        print('Out of time.')
    except pyip.RetryLimitException:
        print('Out of tries.')
    else: 
        print('Correct!')
        corrAns += 1
        
    time.sleep(1)

print('Score is %s / %s' % (corrAns,numQuest))