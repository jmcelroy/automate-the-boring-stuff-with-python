#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 24 19:42:17 2022

@author: jmcelroy
"""

# alarm.wav file opens in vlc
import time, subprocess

timeLeft = 5
while timeLeft > 0:
    print(timeLeft, end='')
    time.sleep(1)
    timeLeft = timeLeft - 1
    
subprocess.Popen(['vlc', '--intf', 'dummy', 'vcd://', 'alarm.wav'])
# https://superuser.com/questions/664826/play-vlc-stream-without-interface 
# used info from above link for a makeshift "--headless" option