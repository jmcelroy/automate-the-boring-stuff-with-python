#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 13 18:47:40 2022

@author: jmcelroy
"""

from pathlib import Path
import shelve
#p = Path('spam.txt')
#p.write_text('Hello, world!')
#print(p.read_text())

# general: open(), read/write(), close()

#helloFile = open('spam.txt', 'r')
#helloContent = helloFile.read()
#print(helloContent)

#print(helloFile.readlines())
#helloFile.close()
#helloFile = open('spam.txt', 'a')
#helloFile.write('\nBacon is yummy.')
#helloFile.close()
#helloFile = open('spam.txt', 'r')
#content = helloFile.read()
#helloFile.close()
#print(content)

#shelfFile = shelve.open('mydata')
#cats = ['A', 'B', 'C']
#shelfFile['cats'] = cats
#shelfFile.close()

shelfFile = shelve.open('mydata')
print(shelfFile['cats'])
shelfFile.close()

