#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun 12 17:57:41 2022

@author: jmcelroy
"""

import pyinputplus as pyip

# prompt="" or '', otherwise it loops with the previous input included
# pyip.inputInt(prompt="Enter a num: ", greaterThan=(7), blank=True, limit=9, timeout=5)

# pyip.inputNum(allowRegexes=([r'(I|V|X|L|C|D|M)+', r'zero']), blockRegexes=(r'[357]$'))

def addsUpToTen(numbers):
    numbersList = list(numbers)
    for i, digit in enumerate(numbersList):
        numbersList[i] = int(digit)
    if sum(numbersList) != 10:
        raise Exception('The digits must add to 10, not %s' %(sum(numbersList)))
    return int(numbers)

pyip.inputCustom(addsUpToTen)
