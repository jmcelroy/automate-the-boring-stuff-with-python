#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 18 08:25:42 2022

@author: jmcelroy
"""

# for searching https://pypi.org/
import requests, sys, webbrowser, bs4

print('Searching...')
res = requests.get('https://pypi.org/search/?q=' + ' '.join(sys.argv[1:]))

soup = bs4.BeautifulSoup(res.text, 'html.parser')
linksElems = soup.select('.package-snippet')

numOpen = min(5, len(linksElems))
for i in range(numOpen):
    urltoopen = 'https://pypi.org' + linksElems[i].get('href')
    print('Opening', urltoopen)
    webbrowser.open(urltoopen)
    
