#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 20 06:30:30 2022

@author: jmcelroy
"""

import docx, readDocx
doc = docx.Document('demo.docx')
#print(len(doc.paragraphs))
#print(doc.paragraphs[0].text)

print(readDocx.getText('demo.docx'))

print(doc.paragraphs[0].style)

doc.paragraphs[0].style = 'Normal'

(doc.paragraphs[1].runs[0].text, doc.paragraphs[1].runs[1].text, 
 doc.paragraphs[1].runs[2].text, doc.paragraphs[1].runs[3].text)

doc.paragraphs[1].runs[0].style = 'QuoteChar'
doc.paragraphs[1].runs[1].underline = True
doc.paragraphs[1].runs[3].underline = True
doc.save('restyled.docx')