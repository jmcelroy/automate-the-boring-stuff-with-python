#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 16 14:58:07 2022

@author: jmcelroy
"""

import zipfile, os
from pathlib import Path

testZip = zipfile.ZipFile(Path.cwd() / 'test_archive.zip')
print(testZip.namelist())

testZipInfo = testZip.getinfo('ch7_reg-exp.py')
print(testZipInfo.file_size)
print(testZipInfo.compress_size)
print(f'Compressed file is {round(testZipInfo.file_size / testZipInfo.compress_size, 2)}x smaller!')
testZip.close()

# .extractall() - into cwd, or specified

newZip = zipfile.ZipFile('new.zip', 'a')
newZip.write('mcb.dat', compress_type=(zipfile.ZIP_DEFLATED))
newZip.close()