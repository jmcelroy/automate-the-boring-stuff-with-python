# variations on stuff on pgs 83-89

import random
import copy

print('Enter # of x\n')
input1 = input()
print('Enter ' + input1 + ' items.\n')

list1 = []
for i in range(int(input1)):
    entry = input()
    list1.insert(i, str(entry))
    # print(list1[i])
    # print(len(list1[i]))

for i in range(len(list1)):
    if len(list1[i]) < 3:
        print('Entry ' + str(i) + ' is invalid.\n')

for i, entry in enumerate(list1):
    print(str(i) + ' is ' + entry)

print(random.choice(list1))
list1.append(random.choice(list1))
# print(len(list1))
print(list1[len(list1)-1])

# .sort(reverse=True) = .reverse()

# tuples immutable lists

print(id(list1))
list2 = copy.copy(list1)
