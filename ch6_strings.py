#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun  3 16:05:06 2022

@author: jmcelroy
"""

import pyperclip

x = r"G:\sys32\bin\folder\ "
print(x)

""" This is a multiline comment. 
Test.
Python 3.
"""

age = 999
print(f'I will be {age + 1} next year.')

# .isupper(), .islower()

print('hello'.rjust(20,'-'))

pyperclip.copy('text')