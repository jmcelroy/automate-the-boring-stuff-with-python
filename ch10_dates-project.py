#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 16 15:37:30 2022

@author: jmcelroy
pgs. 240-3
Changes American-style dates in file names to European-style dates. 
"""

import shutil, os, re

datePattern = re.compile(r"""^(.*?)
                         ((0|1)?\d)-
                         ((0|1|2|3)?\d)-
                         ((19|20)\d\d)
                         (.*?)$
                         """, re.VERBOSE)
                         
for amerFileName in os.listdir('.'):
    mo = datePattern.search(amerFileName)
    
    if mo == None:
        continue
    
    before = mo.group(1)
    month = mo.group(2)
    day = mo.group(4)
    year = mo.group(6)
    after = mo.group(8)
    
    euroFileName = before + day + '-' + month + '-' + year + after

    absWorkingDir = os.path.abspath('.')
    amerFileName = os.path.join(absWorkingDir, amerFileName)
    euroFileName = os.path.join(absWorkingDir, euroFileName)

    print(f'Renaming "{amerFileName}" to "{euroFileName}"...')
    shutil.move(amerFileName, euroFileName)

