#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 17 12:48:17 2022

@author: jmcelroy
"""

import webbrowser, sys, pyperclip

if len(sys.argv) > 1:
    address = ' '.join(sys.argv[1:])
    
#print(sys.argv)

else:
    address = pyperclip.paste()
    
webbrowser.open('https://www.google.com/maps/place/' + address)

