#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  8 06:48:31 2022

@author: jmcelroy
Test page: https://nostarch.com/contactus
"""

import pyperclip, re

phoneRegex = re.compile(r'''(
    (\d{3}|\(d{3}\))?               # area code
    (\s|-|\.?)                      # separator
    (\d{3})                         # 1st 3 digits
    (\s|-|\.)                       # separator
    (\d{4})                         # last 4 digits
    (\s*(ext|x|ext.)\s*(\d{2,5}))?    # extension
    )''', re.VERBOSE)

emailRegex = re.compile(r'''(
    [a-zA-Z0-9._%+-]+               # username
    @                               # 
    [a-zA-Z0-9.-]+                  # domain name
    (\.[a-zA-Z]{2,4})               # dotx
    )''', re.VERBOSE)

# find clipboard matches

text = str(pyperclip.paste())
matches = []
for groups in phoneRegex.findall(text):
    phoneNum = '-'.join([groups[1],groups[3],groups[5]])
    if groups[8] != '':
        phoneNum += ' x' + groups[8]
    matches.append(phoneNum)
for groups in emailRegex.findall(text):
    matches.append(groups[0])
    
# group[0] matches entire reg exp
# append phone #s in standardised format; groups[8] is ext

if len(matches) > 0:
    pyperclip.copy('\n'.join(matches))
    print('Copied to clipboard:')
    print('\n'.join(matches))
else:
    print('No matches found.')

