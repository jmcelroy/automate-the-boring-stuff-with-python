#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 24 16:41:12 2022

@author: jmcelroy
"""

import time


def calcProd():
    product = 1
    for i in range(1, 100000):
        product = product * i
    return product

startTime = time.time()
prod = calcProd()
endTime = time.time()
print('REsult is %s digits long' % len(str(prod)))
print('It took %s seconds to calculate.' % (endTime - startTime))

