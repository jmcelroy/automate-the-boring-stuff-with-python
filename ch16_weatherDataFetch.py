#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 21 20:12:28 2022

@author: jmcelroy
"""

# Updated for current OpenWeatherMap api structure. 
# Shows weather at a given lat/long using your appid. 

import json, requests, sys
if len(sys.argv) < 2:
    print('Usage: ./ch16_weatherDataFetch.py lat long appid')
    sys.exit()
lat = ' '.join(sys.argv[1:2])
long = ' '.join(sys.argv[2:3])
APPID = ' '.join(sys.argv[3:4])

url = 'https://api.openweathermap.org/data/2.5/weather?lat=%s&lon=%s&appid=%s' % (lat, long, APPID)

response = requests.get(url)
response.raise_for_status()

w = json.loads(response.text)

print('Current weather at the coordinates %s %s:' % (lat, long))
print(w['weather'][0]['main'] + ', ' + w['weather'][0]['description'])

temp_celsius = -273.15 + w['main']['temp']

print('The current temperature is %s degrees celsius.' % format(temp_celsius, '.2f'))
print('The current pressure is %s hPa.' % w['main']['pressure'])
print('The city at these coordinates is %s.' % w['name']) 