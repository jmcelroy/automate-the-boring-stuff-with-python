#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 20 06:34:38 2022

@author: jmcelroy
"""

import docx

def getText(filename):
    doc = docx.Document(filename)
    fullText = []
    for para in doc.paragraphs:
        fullText.append('   ' + para.text) # indent paragraphs
    return '\n\n'.join(fullText) # double-spaced lines