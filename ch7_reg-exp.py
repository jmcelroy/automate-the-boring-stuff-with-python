#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun  3 19:28:31 2022

@author: jmcelroy
"""

import re

phoneNumRegex = re.compile(r'(\d{3})-(\d{3}-\d{4})')
mo = phoneNumRegex.search('Number is 111-111-1111.')
print('Phone # found: ' + mo.group())

areaCode, mainNumber = mo.groups()
#print(mo.groups())
print(areaCode)

# ()? optional match, ()* zero or more, ()+ one or more

greedyRegex = re.compile(r'(Ha){3,5}?', re.I)
mo1 = greedyRegex.search('HaHAHaHa')
print(mo1.group())

vowelRegex = re.compile(r'[aeiouAEIOU]')
alphanumericRegex = re.compile(r'[a-zA-Z0-9]')
nonVowelRegex = re.compile(r'[^aeiouAEIOU]')

# table pg. 173

dotStarRegex = re.compile(r'<.*>')
mo2 = dotStarRegex.search('<to serve man> for dinner>')
print(mo2.group())

nonNewLineRegex = re.compile(r'^.*', re.DOTALL)
mo3 = nonNewLineRegex.search('text\ntext\ntext\n')
print(mo3.group())

phoneRegex = re.compile(r'''(
    (\d{3}|\(d{3}\))?               # area code
    (\s|-|\.?)                      # separator
    \d{3}                           # 1st 3 digits
    (\s|-|\.)                       # separator
    \d{4}                           # last 4 digits
    (\s*(ext|x|ext.)\s*\d{2,5})?    # extension
    )''', re.VERBOSE | re.DOTALL)

mo4 = phoneRegex.findall('Number 888-111-8888 random text is 111-111-1111.')
print(mo4)

phoneRegex = re.compile(r'\d{3}-\d{3}-\d{4}')
mo5 = phoneRegex.findall('Number 888-111-8888 random text is 111-111-1111.')
print(mo5)

