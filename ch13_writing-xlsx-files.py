#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 18 18:11:44 2022

@author: jmcelroy
"""

import openpyxl
from openpyxl.styles import Font

wb = openpyxl.Workbook()
sheet = wb.active
sheet.title = 'Spam Bacon Eggs Sheet'
print(wb.sheetnames)

wb.create_sheet(index=1, title='2nd Sheet')
wb['2nd Sheet']['A1'] = 'Hello, world!'

#wb.save('example-workbook.xlsx')
#print(wb['2nd Sheet']['A1'].value)

sheet = wb['2nd Sheet']
italic24Font = Font(name='Times New Roman', size=24, italic=True)
sheet['A1'].font = italic24Font

sheet_1 = wb['Spam Bacon Eggs Sheet']
sheet_1['B2'] = 40
sheet_1['B6'] = 67
sheet_1['B9'] = '=SUM(B1:B8)'

sheet_1.row_dimensions[4].height = 100 # up to 409
sheet_1.column_dimensions['D'].width = 150 # up to 255

sheet_1.merge_cells('F5:H8')

sheet_1['C1'] = 'Header'
bold24Font = Font(name='Times New Roman', size=24, bold=True)
sheet_1['C1'].font = italic24Font
sheet_1.freeze_panes = 'A2'

wb.save('example-workbook.xlsx')