#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 21 17:46:17 2022

@author: jmcelroy
"""

import csv
exampleFile = open('example.csv')
exampleReader = csv.reader(exampleFile)
#exampleData = list(exampleReader)
#print(exampleData)
#print(exampleData[0][2])
for row in exampleReader:
    print('Row #' + str(exampleReader.line_num) + ' ' + str(row))
    
outputFile = open('output.csv', 'w', newline='')
outputWriter = csv.writer(outputFile)
outputWriter.writerow(['spam', 'eggs'])
outputWriter.writerow(['1,2,3,4'])
outputFile.close()

exampleFile2 = open('exampleWithHeader.csv')
exampleDictReader = csv.DictReader(exampleFile2)
for row in exampleDictReader:
    print(row['Timestamp'], row['Fruit'], row['Quantity'])

import json
jsonDataExample = '{"name": "Zophie", "isCat": true, \
                    "miceCaught": 0, "felineIQ": null}'
print(json.loads(jsonDataExample))

