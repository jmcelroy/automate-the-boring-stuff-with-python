# pg 73
import time, sys

indent = 0
inc = True

try:
    while True:
        print(' ' * indent, end='')
        print('*********')
        time.sleep(0.1)

        if inc:
            indent += 1
            if indent == 20:
                inc = False
        else:
            indent -= 1
            if indent == 0:
                inc = True
except KeyboardInterrupt:
    sys.exit(1)
