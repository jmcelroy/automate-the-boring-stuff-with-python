#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 14 15:53:24 2022

@author: jmcelroy
"""
# pg 222
# generates 35 randomised quiz question and answer files

import random, pyinputplus as pyip, time, shelve
from pathlib import Path

capitals = {'Alabama': 'Montgomery',
            'Alaska': 'Juneau', 
            'Arizona': 'Phoenix',
            'Arkansas': 'Little Rock', 
            'California': 'Sacramento', 
            'Colorado': 'Denver',
            'Connecticut': 'Hartford', 
            'Delaware': 'Dover', 
            'Florida': 'Tallahassee',
            'Georgia': 'Atlanta', 
            'Hawaii': 'Honolulu', 
            'Idaho': 'Boise', 
            'Illinois':'Springfield', 
            'Indiana': 'Indianapolis', 
            'Iowa': 'Des Moines', 
            'Kansas':'Topeka', 
            'Kentucky': 'Frankfort', 
            'Louisiana': 'Baton Rouge', 
            'Maine':'Augusta', 
            'Maryland': 'Annapolis', 
            'Massachusetts': 'Boston', 
            'Michigan':'Lansing', 
            'Minnesota': 'Saint Paul', 
            'Mississippi': 'Jackson', 
            'Missouri':'Jefferson City', 
            'Montana': 'Helena', 
            'Nebraska': 'Lincoln', 
            'Nevada':'Carson City', 
            'New Hampshire': 'Concord', 
            'New Jersey': 'Trenton', 
            'New Mexico': 'Santa Fe', 
            'New York': 'Albany', 
            'North Carolina': 'Raleigh',
            'North Dakota': 'Bismarck', 
            'Ohio': 'Columbus', 
            'Oklahoma': 'Oklahoma City',
            'Oregon': 'Salem', 
            'Pennsylvania': 'Harrisburg', 
            'Rhode Island': 'Providence',
            'South Carolina': 'Columbia', 
            'South Dakota': 'Pierre', 
            'Tennessee': 'Nashville', 
            'Texas': 'Austin', 
            'Utah': 'Salt Lake City', 
            'Vermont': 'Montpelier', 
            'Virginia': 'Richmond', 
            'Washington': 'Olympia', 
            'West Virginia': 'Charleston', 
            'Wisconsin': 'Madison', 
            'Wyoming': 'Cheyenne'}

for quizNum in range(35):
    quizFile = open(f'capitalsquiz{quizNum+1}.txt', 'w')
    answerKeyFile = open(f'capitalsquiz_answers{quizNum+1}.txt', 'w')
    
    #header
    quizFile.write('Name:\n\nDate:\n\nPeriod:\n\n')
    quizFile.write((' ' * 20) + f'State Capitols Quiz (Form{quizNum+1})')
    quizFile.write('\n\n')
    #shuffle order
    states = list(capitals.keys())
    random.shuffle(states)
    
    for questionNum in range(50):
        
        corrAns = capitals[states[questionNum]]
        wrongAns = list(capitals.values())
        del wrongAns[wrongAns.index(corrAns)]
        wrongAns = random.sample(wrongAns, 3)
        ansOptions = wrongAns + [corrAns]
        random.shuffle(ansOptions)
        
        quizFile.write(f'{questionNum+1}. What is the capital of {states[questionNum]}?\n')
        
        #question and ans options, 0-3
        for i in range(4):
            # ABCD for 4 options
            quizFile.write(f"    {'ABCD'[i]}. {ansOptions[i]}\n")
        quizFile.write('\n')
        answerKeyFile.write(f"{questionNum+1}. {'ABCD'[ansOptions.index(corrAns)]}")
    
    quizFile.close()
    answerKeyFile.close()

