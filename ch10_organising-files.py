#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 15 19:16:05 2022

@author: jmcelroy
"""

import shutil, os
from pathlib import Path

# copy file
#shutil.copy(Path.cwd()/'spam.txt', Path.cwd()/'testing_dir01')
# copy folder/rename
#shutil.copytree(Path.cwd() / 'testing_dir01', Path.cwd() / 'testing_dir01_backup')

# move() will rename files if folders don't exit - pg 233

#for filename in Path.cwd().glob('*.py'): # to print filenames that would've been deleted
    #os.unlink(filename)
#    print(filename)
    
for folderName, subfolders, filenames in os.walk(Path.cwd()):
    print('The current folder is ' + folderName)
    
    for subfolder in subfolders:
        print('Subfolder of ' + folderName + ': ' + subfolder)
    for filename in filenames:
        print('File inside ' + folderName + ': ' + filename)
    print('')
    
